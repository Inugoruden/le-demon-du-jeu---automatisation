function getContestData(url, callback){
    if (url == null || url == "")
        return;

    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.onreadystatechange = function()
    {
        if(this.readyState == this.DONE && this.status == 200)
        {
            var jsonResponse = JSON.parse(this.responseText);

            jsonResponse.forEach(element => {
                callback(element);
            });
        }
    };

    xhr.send();
}

function openTabs(link)
{
    var chosenPage = '' + link;
    browser.tabs.create({
        url: chosenPage
    });
}

document.addEventListener("click", function(e){
    if (!e.target.classList.contains("page-choice")) {
        return;
    }

    var endpoint = "";

    switch (e.target.id) {
        case "classic":
            endpoint = "http://127.0.0.1:5000/new_contests";
            break;
    
        case "facebook":
            endpoint = "http://127.0.0.1:5000/new_facebook_contests";
            break;

        case "twitter":
            endpoint = "http://127.0.0.1:5000/new_twitter_contests";    
            break;

        default:
            break;
    }

    getContestData(endpoint,openTabs);

});

