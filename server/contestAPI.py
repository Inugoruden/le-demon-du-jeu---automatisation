from flask import Flask, request
import contestdataaccess
import json

app = Flask(__name__)

@app.route("/new_contests", methods=["GET"])
def get_new_contests():
    result = contestdataaccess.get_new_contests()
    contestdataaccess.set_contests_has_shown(result)
    return json.dumps(result)

@app.route("/new_facebook_contests", methods=["GET"])
def get_new_facebook_contests():
    result = contestdataaccess.get_new_Facebook_contests()
    contestdataaccess.set_contests_has_shown(result)
    return json.dumps(result)

@app.route("/new_twitter_contests", methods=["GET"])
def get_new_twitter_contests():
    result = contestdataaccess.get_new_Twitter_contests()
    contestdataaccess.set_contests_has_shown(result)
    return json.dumps(result)
