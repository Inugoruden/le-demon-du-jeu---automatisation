import sqlite3
from datetime import date

DATABASE_FILENAME='contestsdb.db'

def insert_contest(contest):
    try:
        con = sqlite3.connect(DATABASE_FILENAME)
        cur = con.cursor()

        formatted_data=(contest.numero, contest.url, contest.principe, contest.gains, contest.conditions, contest.reponses, contest.date_ajout, contest.date_cloture, contest.isFacebookContest(), contest.isTwitterContest(),0)

        cur.execute('insert into Contest (Numero, Url, Principe, Gains, Conditions, Reponses, DateAjout, DateCloture, isFacebook, isTwitter, alreadyShown) values (?,?,?,?,?,?,?,?,?,?,?)', formatted_data)
        cur.close()

        con.commit()
        con.close()
    except:
        print("An error occured while inserting a contest in {}".format(DATABASE_FILENAME))


def get_new_contests():
    try:
        con = sqlite3.connect(DATABASE_FILENAME)
        cur = con.cursor()

        cur.execute("SELECT Url FROM Contest WHERE alreadyShown=0 and DateCloture=='{}' and isFacebook == 0 and isTwitter == 0 and alreadyShown == 0 limit 10;".format(date.today().strftime("%d/%m/%Y")))

        rows = cur.fetchall()

        cur.close()
        con.commit()
        con.close()

        return rows
    except:
        print("An error occured while getting contests in {}".format(DATABASE_FILENAME))

def get_new_Facebook_contests():
    try:
        con = sqlite3.connect(DATABASE_FILENAME)
        cur = con.cursor()

        cur.execute("SELECT Url FROM Contest WHERE alreadyShown=0 and DateCloture=='{}' and isFacebook == 1 and isTwitter == 0 and alreadyShown == 0 limit 10;".format(date.today().strftime("%d/%m/%Y")))

        rows = cur.fetchall()

        cur.close()
        con.commit()
        con.close()

        return rows
    except:
        print("An error occured while getting contests in {}".format(DATABASE_FILENAME))

def get_new_Twitter_contests():
    try:
        con = sqlite3.connect(DATABASE_FILENAME)
        cur = con.cursor()

        cur.execute("SELECT Url FROM Contest WHERE alreadyShown=0 and DateCloture=='{}' and isFacebook == 0 and isTwitter == 1 and alreadyShown == 0 limit 10;".format(date.today().strftime("%d/%m/%Y")))

        rows = cur.fetchall()

        cur.close()
        con.commit()
        con.close()

        return rows
    except:
        print("An error occured while getting contests in {}".format(DATABASE_FILENAME))


def remove_data():
    try:
        con = sqlite3.connect(DATABASE_FILENAME)
        cur = con.cursor()

        cur.execute("DELETE FROM Contest WHERE 1=1;")

        cur.close()
        con.commit()
        con.close()
    except:
        print("An error occured while removing contests in {}".format(DATABASE_FILENAME))


def set_contests_has_shown(contests):
    if contests is None or len(contests) < 1:
        return

    try:
        con = sqlite3.connect(DATABASE_FILENAME)
        cur = con.cursor()

        for url in contests:
            cur.execute("UPDATE Contest SET alreadyShown = 1 WHERE Url = '{}'".format(url[0]))

        cur.close()
        con.commit()
        con.close()
    except:
        print("An error occured while setting attribute in {}".format(DATABASE_FILENAME))

    