import requests
from bs4 import BeautifulSoup
import re
import contestdataaccess

BASE_URL='https://ledemondujeu.com'

class Contest:
    def __init__(self,numero,date_ajout,date_cloture,principe,conditions,gains,reponses,url):
        self.numero = numero
        self.date_ajout = date_ajout
        self.date_cloture = date_cloture
        self.principe = principe
        self.conditions = conditions
        self.gains = gains
        self.reponses = reponses
        self.url = url

    def __repr__(self):
        return '{} {} {} {} {} {} {} {}'.format(self.numero, self.date_ajout, self.date_cloture, self.principe, self.conditions, self.gains, self.reponses, self.url)

    def __str__(self):
        return '{} {} {} {} {} {} {} {}'.format(self.numero, self.date_ajout, self.date_cloture, self.principe, self.conditions, self.gains, self.reponses, self.url)

    def isFacebookContest(self):
        return self.principe is not None and 'Facebook' in self.principe

    def isTwitterContest(self):
        return self.principe is not None and 'Twitter' in self.principe

# Get contest with end date set to today
def get_near_end_contest():
    contestslist = []
    resp = requests.get('{}/concours-cloture.html'.format(BASE_URL), verify=False)
    resp.encoding = resp.apparent_encoding

    if resp.status_code == 200:
        soup = BeautifulSoup(resp.text, 'html.parser')

        # Get all contests

        for contest in soup.find_all('div', attrs={ 'class' : 'concours' }):
            contestslist.append(parse_contest(contest))

    return contestslist


def parse_contest(contest):
    # Get data and fill a new contest object

    numero=contest.find('div', attrs={ 'class' : 'ficheLien' }).text
    if numero is not None:
        matches = re.findall('[0-9]+',numero)
        numero = matches[0]

    date_ajout=contest.find('div', attrs={ 'class' : 'ficheAjout'}).text
    if date_ajout is not None:
        matches = re.findall('[0-9]{2}/[0-9]{2}/[0-9]{4}',date_ajout)
        date_ajout = matches[0]       

    date_cloture=contest.find('div', attrs={ 'class' : 'ficheCloture'}).text            
    if date_cloture is not None:
        matches = re.findall('[0-9]{2}/[0-9]{2}/[0-9]{4}',date_cloture)
        date_cloture = matches[0]                

    principe=''            
    reponses=''             
    p_or_r=False

    for p in contest.find_all('p', attrs={ 'class' : 'p1' }):
        for span in p.find_all('span'):
            if span.text == 'Principe':
                p_or_r=True
            elif span.text == 'Réponses':
                p_or_r=False
            elif p_or_r == True and span.text != 'Principe':
                principe = span.text
            elif span.text != 'Réponses':
                reponses = span.text            

    conditions=contest.find('p', attrs={ 'class' : 'p1bis' })            
    if conditions is not None:
        for span in conditions.find_all('span'):
            if span.text != 'Conditions':
                conditions = span.text 

    gains=contest.find('p', attrs={ 'class' : 'gains' })            
    if gains is not None:
        for span in gains.find_all('span'):
            if span.text != 'Gains':
                gains = span.text
                 
    url='{}/toconc.php?c={}'.format(BASE_URL,numero)
   
    return Contest(numero,date_ajout,date_cloture,principe,conditions,gains,reponses,url)            


def clear_database():
    contestdataaccess.remove_data()


clear_database()
data = get_near_end_contest()

for c in data:
    contestdataaccess.insert_contest(c)