CREATE TABLE "Contest" (
	"Numero"	INTEGER,
	"Url"	TEXT,
	"Principe"	TEXT,
	"Gains"	TEXT,
	"Conditions"	TEXT,
	"Reponses"	TEXT,
	"DateAjout"	TEXT,
	"DateCloture"	TEXT,
	"isFacebook"	INTEGER,
	"isTwitter"	INTEGER,
	"alreadyShown"	INTEGER
)