# Le Demon du jeu - Automatisation

Ce projet a pour objectif de proposer une solution permettant la récupération de la liste des concours se clotûrant bientôt du site Le Demon du Jeu, ainsi qu'un plugin firefox permettant le chargement de ces concours.

## Pré-requis
### Partie serveur :
*  python3
*  flask
*  requests
*  json
*  BeautifulSoup4
*  sqllite3

### Partie client :
Une version récente de Firefox.

## Utilisation
### Partie serveur :
Une base de données sqlite doit être mise à disposition pour le bon fonctionnement de l'application. Il est possible de modifier le nom de la base dans le script **contestdataaccess.py**.
Il est possible d'initaliser cette base à partir du fichier .sql fourni sur le repository.

**Mise à jour des concours: (à installer dans un cron journalier)**
`python contesthandler.py`

**Utilisation du serveur de développement pour l'API:**
`export FLASK_APP=contestAPI.py` &&
`flask run`

### Partie client :
Il est possible de modifier l'URL de récupération des concours dans le script **choose_contest.js** et les Urls autorisées pour les webrequests dans le manifest.
